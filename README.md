# Test suite verifying correctness of financial service endpoint implemented according to 'BF Seamless Wallet API'

## Installation on Windows and Linux.

We need Node.js and NPM (Node Package Manager) to run JavaScript applications. The best way to manage their versions is by using NVM (Node Version Manager).
Please make sure to have at node version 16.* and npm version 8.*

### Windows:
1. install nvm
  ```sh
  nvm install latest
  ```

2. install and use node
  ```sh
  nvm install 16
  nvm use 16
  ```

3. verify the installation:
  ```sh
  node --version
  ```

  it should return something like:
    ```sh
    v16.15.0
    ```


### Linux:

1. install curl
  ```sh
  sudo apt install curl
  ```

2. install nvm and reload environment
  ```sh
  curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
  source ~/.bashrc
  ```

3. install and use node
  ```sh
  nvm install 16
  nvm use 16
  ```

4. verify the installation:
  ```sh
  node -v && npm -v
  ```

  it should return something like:
  ```sh
  v16.20.2
  8.19.4
  ```

## Ensuring Deterministic Test Results

In order to guarantee the determinism of the test results, tests should be performed on a dedicated account without any other parallel processes.

### Verify Installation:

1. Run the fake implementation of the operator service:

   - Open two terminals in the repository directory (e.g., `~/bf-financial-service-integration$`).
   - In one terminal, type:

     ```sh
     npm start
     ```

     If successful, the terminal will display: `Service listening on port: 8000`

   - In the second terminal, type:

     ```sh
     npm test
     ```

     If everything is configured properly, the terminal will show: `29 passing`

   This indicates that the repository is set up correctly. You can close these test servers and start testing your service.

### Running Tests Against Your Service:

1. Modify data in `test/config.ts` file, ensuring required parameters are correctly set. If integrating via GameHub, set the `gamehub` parameter to `true` (server restart required).

2. In the `token` section, choose one of the following options:
    - Specify a test token to be used for authentication (assigned to the player).
    - Provide a test token endpoint. Create an endpoint that returns a valid token, for example:

      `GET http://your-test-server.com/test-token?balance=1000&playerId=joe&currency=USD`

      Response body: `<token as string>`

3. You can also find an option `debug` in the `test/config.ts` file. This option is responsible for logging. It has to be a boolean value. There is a folder `logs/` which keeps request/response data from the last test run. If the debug option is set to `true`, you will also see logs in the terminal.

4. After updating data in `test/config.ts`, you can start testing your service. I recommend testing methods one by one. Start from authentication, and if all tests pass, move on to `getBalance`, and then to `withdraw`, etc.

5. To isolate specific tests, add `.only` after `describe` to test only one method or after `it` for a single test. To skip a test, use `.skip`.
  ```javascript
  describe.only('authenticateToken', () => {
  // Will run only tests inside this describe
  });

  it.only('authentication valid data', () => {
  // Single test will run
  });
  ```

### Offline Token Construction Guide:

Offline tokens are created using specific data:

- `offlineTokenKey`: Found in `config.ts`
- `caller id`: Found in `config.ts`
- `round_id`: Automatically generated
- `action id`: Automatically generated
- `amount`: Default value of 10 (found in tests as `DEFAULT_DEPOSIT`). Do not use if you're not integrating via GameHub.

To construct the offline token, follow these steps:

1. Concatenate the following data in the correct order (without spaces or pluses):

   `offlineTokenKey` + `caller_id` + `round_id` + `action_id` + `amount` as a string.

2. Compute the SHA224 hash of the concatenated data.

3. Convert the hashed data to hexadecimal (0-9a-f).

4. Append `_` + `'playerId'` to the resulting data.
