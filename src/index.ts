import * as bodyParser from 'body-parser';
import * as crypto from 'crypto';
import * as uuid from 'uuid';

import config from '../test/config';

import BFJSONWSPRequest from './BFJSONWSPRequest';
import { createBFJSONWSPResponse } from './utils';

import express, { Request, Response } from 'express';
const app = express();
app.use(bodyParser.json({ type: '*/json' }));

const PORT = 8000;

let token: string = undefined;
let startToken: string = undefined;
let balance = 0;
let callerId = 'beefee';
let callerPassword = 'SuperPassword!';
let currency = 'EUR';
let playerId = 'joe';
let playerName = 'Joe Doe';
let offlineTokenKey = 'testToken';

let transactions: { [roundIdAndMethod: string]: { amount: number, method: string, actionId: string } } = {};

app.get('/token', (req: Request, res: Response) => {
  token = undefined;
  if (req.query.balance) balance = parseInt(req.query.balance as string);
  if (req.query.currency) currency = req.query.currency as string;
  if (req.query.playerId) playerId = req.query.playerId as string;

  startToken = uuid.v1();
  return res.send(startToken);
});

app.post('/jsonwsp', (req: Request, res: Response) => {
  const request: BFJSONWSPRequest = req.body;
  console.log('Request: ' + JSON.stringify(request));

  if (!request.args) {
    return res.status(400).send('Invalid or missing args');
  };

  if (!callerId || request.args.caller_id !== callerId || !callerPassword || request.args.caller_password !== callerPassword) {
    return res.status(200).json(createBFJSONWSPResponse(request, 2000));
  }

  const invalidToken = (startToken && request.args.token !== startToken) || (token && request.args.token !== token);
  const isOflineDeposit = request.methodname === 'deposit' && request.args.offline === true;
  if (invalidToken && !isOflineDeposit) {
    return res.status(200).json(createBFJSONWSPResponse(request, 2001));
  };

  const amount = request.args ? request.args.amount : 0;
  switch (request.methodname) {

    case 'authenticateToken': {
      startToken = undefined;
      token = uuid.v1();

      return res.status(200).json(createBFJSONWSPResponse(request, 0, { token, balance, currency, player_id: playerId, nickname: playerName }));
    }

    case 'getBalance': {
      if (!request.args.currency || !request.args.game_ref) {
        return res.status(400).send('Invalid or missing currency/game_ref field');
      }

      if (request.args.currency !== currency || !request.args.currency || request.args.currency !== request.args.currency.toLocaleUpperCase()) {
        return res.status(400).send('Invalid or missing currency');
      }
      return res.status(200).json(createBFJSONWSPResponse(request, 0, { token, balance, currency }));
    }

    case 'withdraw': {
      const withdrawTransactionId = request.args.round_id + ' withdraw';
      const depositTransactionId = request.args.round_id + ' deposit';
      const rollbackTransactionId = request.args.round_id + ' rollback';

      const isDepositTransaction = transactions[depositTransactionId]  !== undefined;
      const isRollbackTransaction = transactions[rollbackTransactionId] !== undefined;

      if (withdrawTransactionId in transactions && !isDepositTransaction && !isRollbackTransaction) {
        return res.status(200).json(createBFJSONWSPResponse(request, 3000));
      }

      if (Object.keys(transactions).find(transaction => transactions[transaction].actionId === request.args.action_id) !== undefined) {
        return res.status(200).json(createBFJSONWSPResponse(request, 3000));
      }

      if (isDepositTransaction || isRollbackTransaction) {
        return res.status(200).json(createBFJSONWSPResponse(request, 3002));
      }

      if (balance < amount) {
        return res.status(200).json(createBFJSONWSPResponse(request, 4000));
      }

      transactions[withdrawTransactionId] = { amount: -request.args.amount, method: request.methodname, actionId: request.args.action_id };

      if (request.args.bonus_instance_id === undefined) {
        balance -= request.args.amount;
      }

      return res.status(200).json(createBFJSONWSPResponse(request, 0, { token, balance, currency, transaction_id: withdrawTransactionId }));
    }

    case 'deposit': {
      const withdrawTransactionId = request.args.round_id + ' withdraw';
      const depositTransactionId = request.args.round_id + ' deposit';
      const rollbackTransactionId = request.args.round_id + ' rollback';

      const isDepositTransaction = transactions[depositTransactionId] !== undefined;
      const isRollbackTransaction = transactions[rollbackTransactionId] !== undefined;

      if (Object.keys(transactions).find(transaction => transactions[transaction].actionId === request.args.action_id) !== undefined) {
        return res.status(200).json(createBFJSONWSPResponse(request, 3000));
      }

      if (config.isGamehub) {
        if (transactions[withdrawTransactionId] === undefined && request.args.bonus_instance_id === undefined) {
          return res.status(200).json(createBFJSONWSPResponse(request, 3001));
        }
      } else {
        if (transactions[withdrawTransactionId] === undefined && request.args.bonus_instance_id === undefined && request.args.bonus_id === undefined) {
          return res.status(200).json(createBFJSONWSPResponse(request, 3001));
        }
      }

      if (isDepositTransaction || isRollbackTransaction) {
        return res.status(200).json(createBFJSONWSPResponse(request, 3002));
      }

      if (request.args.offline) {
        let data: string;
        let offlineToken: string;
        if (config.isGamehub) {
          data = `${offlineTokenKey}${callerId}${request.args.round_id}${request.args.action_id}${request.args.amount.toString()}`;
          offlineToken = crypto.createHash('sha224').update(data).digest('hex') + '_' + playerId;
        } else {
          data = `${offlineTokenKey}${callerId}${request.args.round_id}${request.args.action_id}`;
          offlineToken = crypto.createHash('sha224').update(data).digest('hex') + '_' + playerId;
        }

        if (request.args.token !== offlineToken) {
          return res.status(200).json(createBFJSONWSPResponse(request, 2001));
        }

        transactions[depositTransactionId] = { amount: request.args.amount, method: request.methodname, actionId: request.args.action_id };
        balance += request.args.amount;
        return res.status(200).json(createBFJSONWSPResponse(request, 0, { transaction_id: depositTransactionId }));
      };


      transactions[depositTransactionId] = { amount: request.args.amount, method: request.methodname, actionId: request.args.action_id };
      balance += request.args.amount;
      return res.status(200).json(createBFJSONWSPResponse(request, 0, { token, balance, currency, transaction_id: depositTransactionId }));
    }

    case 'rollback': {
      const withdrawTransactionId = request.args.round_id + ' withdraw';
      const depositTransactionId = request.args.round_id + ' deposit';
      const rollbackTransactionId = request.args.round_id + ' rollback';

      const isDepositTransaction = transactions[depositTransactionId] !== undefined;
      const isRollbackTransaction =  transactions[rollbackTransactionId] !== undefined;

      if (Object.keys(transactions).find(transaction => transactions[transaction].actionId === request.args.action_id) !== undefined) {
        return res.status(200).json(createBFJSONWSPResponse(request, 3000));
      }

      if (transactions[withdrawTransactionId] === undefined || request.args.rollback_action_id !== transactions[withdrawTransactionId].actionId) {
        return res.status(200).json(createBFJSONWSPResponse(request, 3001));
      }

      if (isDepositTransaction || isRollbackTransaction) {
        return res.status(200).json(createBFJSONWSPResponse(request, 3002));
      }

      balance -= transactions[withdrawTransactionId].amount;
      transactions[rollbackTransactionId] = { amount: 0, method: request.methodname, actionId: request.args.action_id };
      return res.status(200).json(createBFJSONWSPResponse(request, 0, { token, transaction_id: rollbackTransactionId }));
    }

    case 'tokenRefresh': {
      token = uuid.v1();
      return res.status(200).json(createBFJSONWSPResponse(request, 0, { token }));
    }

    default: {
      return res.status(200).json(createBFJSONWSPResponse(request, 1003));
    }
  };
});

app.listen(PORT, () => {
  console.log(`Service listening on port: ${PORT}`);
});