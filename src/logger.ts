import * as winston from 'winston';
import config from '../test/config';

let transports;

const PATH_TO_LOG = 'logs/wallet_test_log.log';

transports = [new winston.transports.File({
  filename: (PATH_TO_LOG),
  silent: false,
  maxsize: 104857600,
  level: 'debug',
  format: winston.format.combine(
    winston.format.simple(),
  )
}),
new (winston.transports.Console)({
  level: config.isDebug === true ? 'debug' : 'info',
  handleExceptions: true,
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.simple(),
  )
})];

const logger = winston.createLogger({
  transports: transports,
  exitOnError: false,
});

export { logger, PATH_TO_LOG };