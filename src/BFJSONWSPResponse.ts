interface BFJSONWSPResponse {
  methodname: string;
  reflection: {
    id: string;
  };
  servicenumber: number;
  servicename: string;
  result: {
    errorcode?: number;
    token?: string;

    balance?: number;
    currency?: string;

    player_id?: string;
    nickname?: string;
    transaction_id?: string;
  };
  version: string;
  type: string;
}

export default BFJSONWSPResponse;
