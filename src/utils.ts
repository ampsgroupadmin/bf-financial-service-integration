import BFJSONWSPRequest from './BFJSONWSPRequest';
import BFJSONWSPResponse from './BFJSONWSPResponse';

const createBFJSONWSPResponse = (request: BFJSONWSPRequest, errorcode: number, result?: any): BFJSONWSPResponse => ({
  methodname: request.methodname,
  reflection: {
    id: request.mirror.id
  },
  servicenumber: 1,
  servicename: 'BFGamesSeamless',
  result: {
    errorcode,
    ...result
  },
  version: request.version,
  type: 'jsonwsp/response'
});

export {
  createBFJSONWSPResponse
};
