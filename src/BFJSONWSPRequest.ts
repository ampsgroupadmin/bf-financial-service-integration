interface BFJSONWSPRequest {
  args: {
    caller_id: string;
    caller_password: string;
    token: string;
    amount?: number;
    currency?: string;
    game_ref?: string;
    round_id?: string;
    action_id?: string;
    rollback_action_id?: string;
    bonus_id?: string;
    bonus_instance_id?: string;
    operator_id?: string;
    offline?: boolean;
  };
  methodname: string;
  mirror: {
    id: string;
  };
  type: string;
  version: string;
}

export default BFJSONWSPRequest;
