const chakram = require('chakram');
const expect = chakram.expect;
const fs = require('fs');

import * as chai from 'chai';
import chaiExclude from 'chai-exclude';
import * as uuid from 'uuid';

import BFJSONWSPRequest from '../src/BFJSONWSPRequest';
import { logger, PATH_TO_LOG } from '../src/logger';

import config from './config';
import { checkWalletResponse, createRequest, ErrorCodes, HTTPStatusCode, offlineToken, sendToWallet } from './utils';

chai.use(chaiExclude);

const GAME_REF = config.game_ref;
const DEFAULT_WITHDRAW = 100;
const DEFAULT_DEPOSIT = 10;

describe('BeeFeeFinancialService API tests', () => {
  let initialToken: string = undefined;

  before(() => {
    fs.truncate(PATH_TO_LOG, 0, function () { });
    initialToken = config.token;
  });

  beforeEach(async function () {
    if (!config.token) {
      expect(config.tokenUrl).to.not.be.undefined;

      const response = await chakram.get(`${config.tokenUrl}?balance=1000&playerId=${config.playerId}&currency=${config.playerCurrency}`);
      expect(response).to.have.status(HTTPStatusCode.OK);
      initialToken = response.body;
    }

    logger.debug(('====' + this.currentTest.title + '===='));
  });

  describe('general', () => {
    it('try to authenticate with invalid caller_id', async () => {
      const originalRequest = createRequest('authenticateToken', initialToken, {});
      const authenticateRequest = {
        ...originalRequest,
        args: {
          ...originalRequest.args,
          caller_id: 'INVALID_CALLER_ID!'
        }
      };

      logger.debug('Invalid authenticate request');
      logger.debug(JSON.stringify(authenticateRequest));

      const authenticateResponse = await sendToWallet(authenticateRequest);

      logger.debug('Authenticate response');
      logger.debug(JSON.stringify(authenticateResponse));

      checkWalletResponse(authenticateResponse, authenticateRequest, {
        errorcode: ErrorCodes.GENERAL_AUTHENTICATION
      });
    });

    it('use invalid methodName', async () => {
      const invalidMethodRequest = createRequest('INVALID_METHOD_NAME!', initialToken, {});

      logger.debug('Request with invalid methodname');
      logger.debug(JSON.stringify(invalidMethodRequest));

      const invalidMethodResponse = await sendToWallet(invalidMethodRequest);

      logger.debug(invalidMethodRequest.methodname + ' response');
      logger.debug(JSON.stringify(invalidMethodResponse));

      checkWalletResponse(invalidMethodResponse, invalidMethodRequest, {
        errorcode: ErrorCodes.INVALID_METHOD_NAME
      });
    });
  });

  describe('authenticateToken', () => {
    it('validate authenticateToken response data', async () => {
      const authenticateRequest = createRequest('authenticateToken', initialToken, {});

      logger.debug((authenticateRequest.methodname + ' request'));
      logger.debug(JSON.stringify(authenticateRequest));

      const authenticateResponse = await sendToWallet(authenticateRequest);

      logger.debug(authenticateResponse.methodname + ' response');
      logger.debug(JSON.stringify(authenticateResponse));

      checkWalletResponse(authenticateResponse, authenticateRequest, {
        currency: config.playerCurrency,
        player_id: config.playerId,
        balance: authenticateResponse.result.balance
      });
    });

    it('check that authentication is impossible when invalid token is send', async () => {
      const authenticateRequest = createRequest('authenticateToken', 'INVALID_TOKEN', {});

      logger.debug((authenticateRequest.methodname + ' request'));
      logger.debug(JSON.stringify(authenticateRequest));

      const authenticateResponse = await sendToWallet(authenticateRequest);

      logger.debug((authenticateRequest.methodname + ' response'));
      logger.debug(JSON.stringify(authenticateResponse));

      checkWalletResponse(authenticateResponse, authenticateRequest, {
        errorcode: ErrorCodes.INVALID_TOKEN
      });
    });
  });

  describe('getBalance', () => {
    it('getBalance with not authenticated token', async () => {
      const authenticateRequest = createRequest('authenticateToken', 'INVALID_TOKEN', {});
      const authenticateResponse = await sendToWallet(authenticateRequest);

      const balanceRequest = createRequest('getBalance', authenticateResponse.result.token, {
        currency: config.playerCurrency,
        game_ref: GAME_REF,
      });

      logger.debug(balanceRequest.methodname + ' request');
      logger.debug(JSON.stringify(balanceRequest));

      const getBalanceResponse = await sendToWallet(balanceRequest);

      logger.debug(balanceRequest.methodname + ' response');
      logger.debug(JSON.stringify(getBalanceResponse));


      checkWalletResponse(getBalanceResponse, balanceRequest, {
        errorcode: ErrorCodes.INVALID_TOKEN
      });
    });

    it('check that getBalance data is valid', async () => {
      const authenticateRequest = createRequest('authenticateToken', initialToken, {});
      const authenticateResponse = await sendToWallet(authenticateRequest);

      const balanceRequest = createRequest('getBalance', authenticateResponse.result.token, {
        currency: config.playerCurrency,
        game_ref: GAME_REF,
      });

      logger.debug(balanceRequest.methodname + ' request');
      logger.debug(JSON.stringify(balanceRequest));

      const getBalanceResponse = await sendToWallet(balanceRequest);

      logger.debug(balanceRequest.methodname + ' response');
      logger.debug(JSON.stringify(getBalanceResponse));

      checkWalletResponse(getBalanceResponse, balanceRequest, {
        balance: authenticateResponse.result.balance,
        currency: config.playerCurrency,
      });
    });
  });

  describe('withdraw', () => {
    let token: string = undefined;
    let balanceAfterAuthenticate: number = undefined;

    beforeEach(async () => {
      const authenticateRequest = createRequest('authenticateToken', initialToken, {});
      const authenticateResponse = await sendToWallet(authenticateRequest);

      token = authenticateResponse.result.token;
      balanceAfterAuthenticate = authenticateResponse.result.balance;
    });

    it('validate withdraw response data', async () => {
      const withdrawRequest = createRequest('withdraw', token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: uuid.v1()
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const withdrawResponse = await sendToWallet(withdrawRequest);

      logger.debug(withdrawRequest.methodname + ' response');
      logger.debug(JSON.stringify(withdrawResponse));


      checkWalletResponse(withdrawResponse, withdrawRequest, {
        balance: balanceAfterAuthenticate - DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
      });
    });

    it('call withdraw twice with same action_id', async () => {
      const action_id = uuid.v1();

      const withdrawRequest = createRequest('withdraw', token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: action_id
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const firstWithdrawResponse = await sendToWallet(withdrawRequest);

      logger.debug('First' + withdrawRequest.methodname + ' response');
      logger.debug(JSON.stringify(firstWithdrawResponse));

      checkWalletResponse(firstWithdrawResponse, withdrawRequest, {
        balance: balanceAfterAuthenticate - DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
      });

      const secondWithdrawRequest = createRequest('withdraw', token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: action_id
      });

      logger.debug('Second ' + secondWithdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(secondWithdrawRequest));

      let secondWithdrawResponse = await sendToWallet(secondWithdrawRequest);

      logger.debug('Second ' + secondWithdrawRequest.methodname + ' response');
      logger.debug(JSON.stringify(secondWithdrawResponse));

      if (secondWithdrawResponse.result.errorcode !== 0 && secondWithdrawResponse.result.errorcode !== undefined) {
        checkWalletResponse(secondWithdrawResponse, secondWithdrawRequest, {
          errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED,
        });
      } else {
        expect(secondWithdrawResponse.reflection).to.deep.equal(secondWithdrawRequest.mirror);
        expect(secondWithdrawResponse.result.transaction_id).to.be.not.empty;
        expect(secondWithdrawResponse).excludingEvery(['reflection', 'transaction_id']).to.deep.equal(firstWithdrawResponse);
      }
    });

    it('call withdraw twice with same round_id', async () => {
      const round_id = uuid.v1();

      const withdrawRequest = createRequest('withdraw', token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: round_id,
        action_id: uuid.v1()
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const firstWithdrawResponse = await sendToWallet(withdrawRequest);

      logger.debug('First' + withdrawRequest.methodname + ' response');
      logger.debug(JSON.stringify(firstWithdrawResponse));

      checkWalletResponse(firstWithdrawResponse, withdrawRequest, {
        balance: balanceAfterAuthenticate - DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
      });

      const secondWithdrawRequest = createRequest('withdraw', token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: round_id,
        action_id: uuid.v1()
      });

      logger.debug('Second ' + secondWithdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(secondWithdrawRequest));

      const secondWithdrawResponse = await sendToWallet(secondWithdrawRequest);

      logger.debug('Second ' + secondWithdrawRequest.methodname + ' response');
      logger.debug(JSON.stringify(secondWithdrawResponse));

      if (secondWithdrawResponse.result.errorcode !== 0 && secondWithdrawResponse.result.errorcode !== undefined) {
        checkWalletResponse(secondWithdrawResponse, secondWithdrawRequest, {
          errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED,
        });
      } else {
        expect(secondWithdrawResponse.reflection).to.deep.equal(secondWithdrawRequest.mirror);
        expect(secondWithdrawResponse.result.transaction_id).to.be.not.empty;
        expect(secondWithdrawResponse).excludingEvery(['reflection', 'transaction_id']).to.deep.equal(firstWithdrawResponse);
      }
    });


    it('two withdraws will return two different transaction_id', async () => {
      const firstWithdrawRequest = createRequest('withdraw', token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: uuid.v1()
      });

      logger.debug(firstWithdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(firstWithdrawRequest));

      const firstWithdrawResponse = await sendToWallet(firstWithdrawRequest);

      logger.debug('First' + firstWithdrawRequest.methodname + ' response');
      logger.debug(JSON.stringify(firstWithdrawResponse));

      const secondWithdrawRequest = createRequest('withdraw', token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: uuid.v1()
      });

      logger.debug('Second ' + secondWithdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(secondWithdrawRequest));

      const secondWithdrawResponse = await sendToWallet(secondWithdrawRequest);

      logger.debug('Second ' + secondWithdrawRequest.methodname + ' response');
      logger.debug(JSON.stringify(secondWithdrawResponse));

      expect(firstWithdrawResponse.result.transaction_id).to.not.equal(secondWithdrawResponse.result.transaction_id);
    });

    it('zero amount withdraw', async () => {
      const withdrawRequest = createRequest('withdraw', token, {
        amount: 0,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: uuid.v1()
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const withdrawResponse = await sendToWallet(withdrawRequest);

      logger.debug(withdrawRequest.methodname + ' response');
      logger.debug(JSON.stringify(withdrawResponse));

      checkWalletResponse(withdrawResponse, withdrawRequest, {
        balance: balanceAfterAuthenticate,
        currency: config.playerCurrency,
      });
    });

    it('withdraw more than in balance', async () => {
      const withdrawRequest = createRequest('withdraw', token, {
        amount: balanceAfterAuthenticate + 1,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: uuid.v1()
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const withdrawResponse = await sendToWallet(withdrawRequest);

      logger.debug(withdrawResponse.methodname + ' response');
      logger.debug(JSON.stringify(withdrawResponse));

      checkWalletResponse(withdrawResponse, withdrawRequest, {
        errorcode: ErrorCodes.PLAYER_INSUFFICIENT_FUNDS,
      });
    });
  });

  describe('deposit', () => {
    let roundId: string = undefined;
    let token: string = undefined;
    let balanceAfterWithdraw: number = undefined;
    let rollbackActionId: string = undefined;
    let authenticateResponse: any = undefined;

    beforeEach(async () => {
      roundId = uuid.v1();

      const authenticateRequest = createRequest('authenticateToken', initialToken, {});
      authenticateResponse = await sendToWallet(authenticateRequest);
      token = authenticateResponse.result.token;

      const withdrawRequest = createRequest('withdraw', authenticateResponse.result.token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const withdrawResponse = await sendToWallet(withdrawRequest);

      logger.debug(withdrawResponse.methodname + ' response');
      logger.debug(JSON.stringify(withdrawResponse));

      balanceAfterWithdraw = withdrawResponse.result.balance;
      rollbackActionId = withdrawRequest.args.action_id;
    });

    it('validate deposit response data', async () => {
      const depositRequest = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(depositRequest.methodname + ' request');
      logger.debug(JSON.stringify(depositRequest));

      const depositResponse = await sendToWallet(depositRequest);

      logger.debug(depositRequest.methodname + ' response');
      logger.debug(JSON.stringify(depositResponse));

      checkWalletResponse(depositResponse, depositRequest, {
        currency: config.playerCurrency,
        balance: balanceAfterWithdraw + DEFAULT_DEPOSIT
      });
    });

    it('deposit action without matching withdraw', async () => {
      const depositRequest = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: uuid.v1(),
      });

      logger.debug(depositRequest.methodname + ' request');
      logger.debug(JSON.stringify(depositRequest));

      const depositResponse = await sendToWallet(depositRequest);

      logger.debug(depositRequest.methodname + ' response');
      logger.debug(JSON.stringify(depositResponse));

      checkWalletResponse(depositResponse, depositRequest, {
        errorcode: ErrorCodes.TRANSACTION_NOT_FOUND
      });
    });

    // Bonus functionality tests are skipped for now.
    (config.isBonusFunctionality ? it.skip : it.skip)('deposit action with bonus without matching withdraw', async () => {
      let depositRequest: BFJSONWSPRequest;
      depositRequest = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: uuid.v1(),
        bonus_id: (config.isGamehub === true ? undefined : config.bonusProgramId),
        bonus_program_id: (config.isGamehub === true ? config.bonusProgramId: undefined),
        bonus_instance_id: config.bonusInstanceId,
        bonus_system_id: (config.isGamehub === true ? config.bonusSystemId: undefined)
      });

      logger.debug(depositRequest.methodname + ' request');
      logger.debug(JSON.stringify(depositRequest));

      const depositResponse = await sendToWallet(depositRequest);

      logger.debug(depositRequest.methodname + ' response');
      logger.debug(JSON.stringify(depositResponse));

      checkWalletResponse(depositResponse, depositRequest, {
        currency: config.playerCurrency,
        balance: balanceAfterWithdraw + DEFAULT_DEPOSIT
      });
    });

    it('deposit twice with same action_id', async () => {
      const depositRequest = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(depositRequest.methodname + ' request');
      logger.debug(JSON.stringify(depositRequest));

      const firstDepositResponse = await sendToWallet(depositRequest);

      logger.debug('First' + depositRequest.methodname + ' response');
      logger.debug(JSON.stringify(firstDepositResponse));

      checkWalletResponse(firstDepositResponse, depositRequest, {
        balance: balanceAfterWithdraw + DEFAULT_DEPOSIT,
        currency: config.playerCurrency
      });

      const secondDepositResponse = await sendToWallet(depositRequest);

      logger.debug('Second ' + depositRequest.methodname + ' response');
      logger.debug(JSON.stringify(secondDepositResponse));

      if (secondDepositResponse.result.errorcode !== 0 && secondDepositResponse.result.errorcode !== undefined) {
        checkWalletResponse(secondDepositResponse, depositRequest, {
          errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED
        });
      } else {
        expect(secondDepositResponse).to.deep.equal(firstDepositResponse);
      }

      const balanceRequest = createRequest('getBalance', token, {
        currency: config.playerCurrency,
        game_ref: GAME_REF,
      });

      logger.debug(balanceRequest.methodname + ' request');
      logger.debug(JSON.stringify(balanceRequest));

      const getBalanceResponse = await sendToWallet(balanceRequest);

      logger.debug(balanceRequest.methodname + ' response');
      logger.debug(JSON.stringify(getBalanceResponse));

      checkWalletResponse(getBalanceResponse, balanceRequest, {
        balance: firstDepositResponse.result.balance,
        currency: config.playerCurrency,
      });
    });

    it('deposit twice with different round_id with withdraw action between deposits', async () => {
      const firstDepositRequest = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug('First' + firstDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(firstDepositRequest));

      const firstDepositResponse = await sendToWallet(firstDepositRequest);

      logger.debug('First' + firstDepositRequest.methodname + ' response');
      logger.debug(JSON.stringify(firstDepositResponse));

      checkWalletResponse(firstDepositResponse, firstDepositRequest, {
        balance: balanceAfterWithdraw + DEFAULT_DEPOSIT,
        currency: config.playerCurrency
      });

      const withdrawRequest = createRequest('withdraw', token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: uuid.v1()
      });

      const secondWithdrawResponse = await sendToWallet(withdrawRequest);
      const secondDepositRequest = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: withdrawRequest.args.round_id,
        action_id: uuid.v1()
      });

      logger.debug('Second ' + secondDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(secondDepositRequest));

      const secondDepositResponse = await sendToWallet(secondDepositRequest);

      logger.debug('Second ' + secondDepositResponse.methodname + ' response');
      logger.debug(JSON.stringify(secondDepositResponse));

      checkWalletResponse(secondDepositResponse, secondDepositRequest, {
        balance: secondWithdrawResponse.result.balance + DEFAULT_DEPOSIT,
        currency: config.playerCurrency
      });
    });

    it('deposit zero', async () => {
      const depositRequest = createRequest('deposit', token, {
        amount: 0,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(depositRequest.methodname + ' request');
      logger.debug(JSON.stringify(depositRequest));

      const depositResponse = await sendToWallet(depositRequest);

      logger.debug(depositResponse.methodname + ' response');
      logger.debug(JSON.stringify(depositResponse));

      checkWalletResponse(depositResponse, depositRequest, {
        balance: balanceAfterWithdraw,
        currency: config.playerCurrency
      });
    });

    // Bonus functionality tests are skipped for now.
    (config.isBonusFunctionality ? it.skip : it.skip)('deposit action with bonus without matching withdraw triggers end of the round, withdraw on ended round', async () => {
      let depositRequestWithBonus: BFJSONWSPRequest;
      const roundId = uuid.v1();
      depositRequestWithBonus = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1(),
        bonus_id: (config.isGamehub === true ? undefined : config.bonusProgramId),
        bonus_program_id: (config.isGamehub === true ? config.bonusProgramId: undefined),
        bonus_instance_id: config.bonusInstanceId,
        bonus_system_id: (config.isGamehub === true ? config.bonusSystemId: undefined)
      });

      logger.debug(depositRequestWithBonus.methodname + ' request');
      logger.debug(JSON.stringify(depositRequestWithBonus));

      const depositResponseWithBonus = await sendToWallet(depositRequestWithBonus);

      logger.debug(depositResponseWithBonus.methodname + ' response');
      logger.debug(JSON.stringify(depositResponseWithBonus));

      checkWalletResponse(depositResponseWithBonus, depositRequestWithBonus, {
        balance: balanceAfterWithdraw + DEFAULT_DEPOSIT,
        currency: config.playerCurrency
      });

      // withdraw on ended round
      const withdrawRequest = createRequest('withdraw', authenticateResponse.result.token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const withdrawResponse = await sendToWallet(withdrawRequest);
      logger.debug(withdrawResponse.methodname + ' response');
      logger.debug(JSON.stringify(withdrawResponse));

      checkWalletResponse(withdrawResponse, withdrawRequest,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });

    it('deposit with matching withdraw triggers end of the round, withdraw on ended round', async () => {
      const depositRequestOnWithdraw = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(depositRequestOnWithdraw.methodname + ' request');
      logger.debug(JSON.stringify(depositRequestOnWithdraw));

      const depositResponseOnWithdraw = await sendToWallet(depositRequestOnWithdraw);

      logger.debug(depositResponseOnWithdraw.methodname + ' response');
      logger.debug(JSON.stringify(depositResponseOnWithdraw));

      checkWalletResponse(depositResponseOnWithdraw, depositRequestOnWithdraw, {
        balance: balanceAfterWithdraw + DEFAULT_DEPOSIT,
        currency: config.playerCurrency
      });

      // withdraw on ended round
      const withdrawRequest = createRequest('withdraw', authenticateResponse.result.token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const withdrawResponse = await sendToWallet(withdrawRequest);
      logger.debug(withdrawResponse.methodname + ' response');
      logger.debug(JSON.stringify(withdrawResponse));

      checkWalletResponse(withdrawResponse, withdrawRequest,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });

    it('deposit with matching withdraw triggers end of the round, offline deposit on ended round', async () => {
      const depositRequestOnWithdraw = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(depositRequestOnWithdraw.methodname + ' request');
      logger.debug(JSON.stringify(depositRequestOnWithdraw));

      const depositResponseOnWithdraw = await sendToWallet(depositRequestOnWithdraw);

      logger.debug(depositResponseOnWithdraw.methodname + ' response');
      logger.debug(JSON.stringify(depositResponseOnWithdraw));

      checkWalletResponse(depositResponseOnWithdraw, depositRequestOnWithdraw, {
        balance: balanceAfterWithdraw + DEFAULT_DEPOSIT,
        currency: config.playerCurrency
      });

      // offlineDeposit on ended round
      const depositActionId = uuid.v1();
      let offlineDepositRequest: BFJSONWSPRequest;
      offlineDepositRequest = createRequest('deposit', offlineToken(roundId, depositActionId, (config.isGamehub ? DEFAULT_DEPOSIT : undefined)), {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: depositActionId,
        offline: true
      });

      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const offlineDepositResponse = await sendToWallet(offlineDepositRequest);

      logger.debug(offlineDepositRequest.methodname + ' response');
      logger.debug(JSON.stringify(offlineDepositResponse));

      checkWalletResponse(offlineDepositResponse, offlineDepositRequest,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });

    it('deposit with matching withdraw triggers end of the round, deposit on ended round', async () => {
      const depositRequestOnWithdraw = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(depositRequestOnWithdraw.methodname + ' request');
      logger.debug(JSON.stringify(depositRequestOnWithdraw));

      const depositResponseOnWithdraw = await sendToWallet(depositRequestOnWithdraw);

      logger.debug(depositResponseOnWithdraw.methodname + ' response');
      logger.debug(JSON.stringify(depositResponseOnWithdraw));

      checkWalletResponse(depositResponseOnWithdraw, depositRequestOnWithdraw, {
        balance: balanceAfterWithdraw + DEFAULT_DEPOSIT,
        currency: config.playerCurrency
      });

      // deposit on ended round
      const depositRequest = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(depositRequest.methodname + ' request');
      logger.debug(JSON.stringify(depositRequest));

      const depositResponse = await sendToWallet(depositRequest);

      logger.debug(depositRequest.methodname + ' response');
      logger.debug(JSON.stringify(depositResponse));

      checkWalletResponse(depositResponse, depositRequest,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });

    it('deposit with matching withdraw triggers end of the round, rollback on ended round', async () => {
      const depositRequestOnWithdraw = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(depositRequestOnWithdraw.methodname + ' request');
      logger.debug(JSON.stringify(depositRequestOnWithdraw));

      const depositResponseOnWithdraw = await sendToWallet(depositRequestOnWithdraw);

      logger.debug(depositResponseOnWithdraw.methodname + ' response');
      logger.debug(JSON.stringify(depositResponseOnWithdraw));

      checkWalletResponse(depositResponseOnWithdraw, depositRequestOnWithdraw, {
        balance: balanceAfterWithdraw + DEFAULT_DEPOSIT,
        currency: config.playerCurrency
      });

      // rollback on ended round
      const rollbackRequestOnEndedRound = createRequest('rollback', token, {
        round_id: roundId,
        rollback_action_id: rollbackActionId,
        action_id: uuid.v1(),
        game_ref: GAME_REF
      });

      logger.debug(rollbackRequestOnEndedRound.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequestOnEndedRound));

      const rollbackResponseFromEndedRound = await sendToWallet(rollbackRequestOnEndedRound);
      logger.debug(rollbackResponseFromEndedRound.methodname + ' response');

      logger.debug(JSON.stringify(rollbackResponseFromEndedRound));

      checkWalletResponse(rollbackResponseFromEndedRound, rollbackRequestOnEndedRound,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });
  });

  describe('rollback', () => {
    let token: string = undefined;
    let roundId: string = undefined;
    let rollbackActionId: string = undefined;
    let balance: number = undefined;

    beforeEach(async () => {
      const authenticateRequest = createRequest('authenticateToken', initialToken, {});
      const authenticateResponse = await sendToWallet(authenticateRequest);

      token = authenticateResponse.result.token;
      balance = authenticateResponse.result.balance;
      const withdrawRequest = createRequest('withdraw', authenticateResponse.result.token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: uuid.v1()
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const withdrawResponse = await sendToWallet(withdrawRequest);

      logger.debug(withdrawResponse.methodname + ' response');
      logger.debug(JSON.stringify(withdrawResponse));

      roundId = withdrawRequest.args.round_id;
      rollbackActionId = withdrawRequest.args.action_id;
    });

    it('it is possible to rollback valid rollback_action_id', async () => {
      const rollbackRequest = createRequest('rollback', token, {
        round_id: roundId,
        rollback_action_id: rollbackActionId,
        action_id: uuid.v1(),
        game_ref: GAME_REF
      });

      logger.debug(rollbackRequest.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequest));

      const rollbackResponse = await sendToWallet(rollbackRequest);

      logger.debug(rollbackResponse.methodname + ' response');
      logger.debug(JSON.stringify(rollbackResponse));

      checkWalletResponse(rollbackResponse, rollbackRequest);
    });

    it('rollback twice same action_id', async () => {
      const rollbackRequest = createRequest('rollback', token, {
        round_id: roundId,
        rollback_action_id: rollbackActionId,
        action_id: uuid.v1(),
        game_ref: GAME_REF
      });

      logger.debug(rollbackRequest.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequest));

      const firstRollbackResponse = await sendToWallet(rollbackRequest);

      logger.debug('First' + firstRollbackResponse.methodname + ' response');
      logger.debug(JSON.stringify(firstRollbackResponse));

      checkWalletResponse(firstRollbackResponse, rollbackRequest);

      const secondRollbackResponse = await sendToWallet(rollbackRequest);

      logger.debug('Second ' + secondRollbackResponse.methodname + ' response');
      logger.debug(JSON.stringify(secondRollbackResponse));

      if (secondRollbackResponse.result.errorcode !== 0 && secondRollbackResponse.result.errorcode !== undefined) {
        checkWalletResponse(secondRollbackResponse, rollbackRequest, {
          errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED
        });
      } else {
        expect(secondRollbackResponse).to.deep.equal(firstRollbackResponse);
      }

      const balanceRequest = createRequest('getBalance', token, {
        currency: config.playerCurrency,
        game_ref: GAME_REF,
      });

      logger.debug(balanceRequest.methodname + ' request');
      logger.debug(JSON.stringify(balanceRequest));

      const getBalanceResponse = await sendToWallet(balanceRequest);

      logger.debug(balanceRequest.methodname + ' response');
      logger.debug(JSON.stringify(getBalanceResponse));

      checkWalletResponse(getBalanceResponse, balanceRequest, {
        balance: balance,
        currency: config.playerCurrency,
      });
    });

    it('rollback two different valid round/rollback_action id pairs with same action_id', async () => {
      const actionId = uuid.v1();

      const rollbackRequest = createRequest('rollback', token, {
        round_id: roundId,
        rollback_action_id: rollbackActionId,
        action_id: actionId,
        game_ref: GAME_REF
      });

      logger.debug('First' + rollbackRequest.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequest));

      const firstRollbackResponse = await sendToWallet(rollbackRequest);

      logger.debug('First' + firstRollbackResponse.methodname + ' response');
      logger.debug(JSON.stringify(firstRollbackResponse));

      checkWalletResponse(firstRollbackResponse, rollbackRequest);

      const withdrawRequest = createRequest('withdraw', token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: uuid.v1(),
        action_id: uuid.v1()
      });

      logger.debug('Second ' + withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const withdrawResponse = await sendToWallet(withdrawRequest);

      logger.debug('Second ' + withdrawResponse.methodname + ' request');
      logger.debug(JSON.stringify(withdrawResponse));

      const secondRollbackRequest = createRequest('rollback', token, {
        round_id: withdrawRequest.args.round_id,
        rollback_action_id: withdrawRequest.args.action_id,
        action_id: actionId,
        game_ref: GAME_REF
      });

      logger.debug('Second ' + secondRollbackRequest.methodname + ' request');
      logger.debug(JSON.stringify(secondRollbackRequest));

      const secondRollbackResponse = await sendToWallet(secondRollbackRequest);

      logger.debug('Second ' + secondRollbackResponse.methodname + ' response');
      logger.debug(JSON.stringify(secondRollbackResponse));
      if (secondRollbackResponse.result.errorcode !== 0 && secondRollbackResponse.result.errorcode !== undefined) {
        checkWalletResponse(secondRollbackResponse, secondRollbackRequest, {
          errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED,
        });
      } else {
        expect(secondRollbackResponse.reflection).to.deep.equal(secondRollbackRequest.mirror);
        expect(secondRollbackResponse.result.transaction_id).to.be.not.empty;
        expect(secondRollbackResponse).excludingEvery(['reflection', 'transaction_id']).to.deep.equal(firstRollbackResponse);
      }
    });

    it('rollback with correct roundId and incorrect rollback_action_id', async () => {
      const rollbackRequest = createRequest('rollback', token, {
        round_id: roundId,
        rollback_action_id: 'no match here',
        action_id: uuid.v1(),
        game_ref: GAME_REF
      });

      logger.debug(rollbackRequest.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequest));

      const rollbackResponse = await sendToWallet(rollbackRequest);

      logger.debug(rollbackResponse.methodname + ' response');
      logger.debug(JSON.stringify(rollbackResponse));

      checkWalletResponse(rollbackResponse, rollbackRequest, {
        errorcode: ErrorCodes.TRANSACTION_NOT_FOUND,
      });
    });

    it('rollback with correct rollback_action_id and incorrect roundId', async () => {
      const rollbackRequest = createRequest('rollback', token, {
        round_id: 'incorrect roundId',
        rollback_action_id: rollbackActionId,
        action_id: uuid.v1(),
        game_ref: GAME_REF
      });

      logger.debug(rollbackRequest.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequest));

      const rollbackResponse = await sendToWallet(rollbackRequest);

      logger.debug(rollbackResponse.methodname + ' response');
      logger.debug(JSON.stringify(rollbackResponse));

      checkWalletResponse(rollbackResponse, rollbackRequest, {
        errorcode: ErrorCodes.TRANSACTION_NOT_FOUND,
      });
    });

    it('rollback with withdraw triggers end of the round, offline deposit on ended round', async () => {
      const rollbackRequest = createRequest('rollback', token, {
        round_id: roundId,
        rollback_action_id: rollbackActionId,
        action_id: uuid.v1(),
        game_ref: GAME_REF
      });
      logger.debug(rollbackRequest.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequest));

      const rollbackResponse = await sendToWallet(rollbackRequest);

      logger.debug(rollbackResponse.methodname + ' response');
      logger.debug(JSON.stringify(rollbackResponse));

      checkWalletResponse(rollbackResponse, rollbackRequest);

      // offlineDeposit on ended round
      const depositActionId = uuid.v1();
      let offlineDepositRequest: BFJSONWSPRequest;
      offlineDepositRequest = createRequest('deposit', offlineToken(roundId, depositActionId, (config.isGamehub ? DEFAULT_DEPOSIT : undefined)), {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: depositActionId,
        offline: true
      });

      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const offlineDepositResponse = await sendToWallet(offlineDepositRequest);

      logger.debug(offlineDepositRequest.methodname + ' response');
      logger.debug(JSON.stringify(offlineDepositResponse));

      checkWalletResponse(offlineDepositResponse, offlineDepositRequest,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });

    it('rollback with withdraw triggers end of the round, deposit on ended round', async () => {
      const rollbackRequest = createRequest('rollback', token, {
        round_id: roundId,
        rollback_action_id: rollbackActionId,
        action_id: uuid.v1(),
        game_ref: GAME_REF
      });
      logger.debug(rollbackRequest.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequest));

      const rollbackResponse = await sendToWallet(rollbackRequest);

      logger.debug(rollbackResponse.methodname + ' response');
      logger.debug(JSON.stringify(rollbackResponse));

      checkWalletResponse(rollbackResponse, rollbackRequest);

      // deposit on ended round
      const depositRequest = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(depositRequest.methodname + ' request');
      logger.debug(JSON.stringify(depositRequest));

      const depositResponse = await sendToWallet(depositRequest);

      logger.debug(depositRequest.methodname + ' response');
      logger.debug(JSON.stringify(depositResponse));

      checkWalletResponse(depositResponse, depositRequest,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });

    it('rollback with withdraw triggers end of the round, rollback on ended round', async () => {
      const rollbackRequest = createRequest('rollback', token, {
        round_id: roundId,
        rollback_action_id: rollbackActionId,
        action_id: uuid.v1(),
        game_ref: GAME_REF
      });
      logger.debug(rollbackRequest.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequest));

      const rollbackResponse = await sendToWallet(rollbackRequest);

      logger.debug(rollbackResponse.methodname + ' response');
      logger.debug(JSON.stringify(rollbackResponse));

      checkWalletResponse(rollbackResponse, rollbackRequest);

      // rollback on ended round
      const rollbackRequestOnEndedRound = createRequest('rollback', token, {
        round_id: roundId,
        rollback_action_id: rollbackActionId,
        action_id: uuid.v1(),
        game_ref: GAME_REF
      });

      logger.debug(rollbackRequestOnEndedRound.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequestOnEndedRound));

      const rollbackResponseFromEndedRound = await sendToWallet(rollbackRequestOnEndedRound);
      logger.debug(rollbackResponseFromEndedRound.methodname + ' response');

      logger.debug(JSON.stringify(rollbackResponseFromEndedRound));

      checkWalletResponse(rollbackResponseFromEndedRound, rollbackRequestOnEndedRound,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });

    it('rollback with withdraw triggers end of the round, withdraw on ended round', async () => {
      const rollbackRequest = createRequest('rollback', token, {
        round_id: roundId,
        rollback_action_id: rollbackActionId,
        action_id: uuid.v1(),
        game_ref: GAME_REF
      });
      logger.debug(rollbackRequest.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequest));

      const rollbackResponse = await sendToWallet(rollbackRequest);

      logger.debug(rollbackResponse.methodname + ' response');
      logger.debug(JSON.stringify(rollbackResponse));

      checkWalletResponse(rollbackResponse, rollbackRequest);

      // withdraw  on ended round
      const withdrawRequest = createRequest('withdraw', token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const withdrawResponse = await sendToWallet(withdrawRequest);
      logger.debug(withdrawResponse.methodname + ' response');
      logger.debug(JSON.stringify(withdrawResponse));

      checkWalletResponse(withdrawResponse, withdrawRequest,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });
  });

  describe('tokenRefresh', () => {
    let token: string = undefined;
    let balanceAfterAuthenticate: number = undefined;
    let refreshResponseToken: string = undefined;

    beforeEach(async () => {
      const authenticateRequest = createRequest('authenticateToken', initialToken, {});
      const authenticateResponse = await sendToWallet(authenticateRequest);
      token = authenticateResponse.result.token;
      balanceAfterAuthenticate = authenticateResponse.result.balance;
    });

    afterEach(() => {
      if (initialToken === token && initialToken !== refreshResponseToken ) {
        initialToken = refreshResponseToken;
      }
    });

    it('validate tokenRefresh response data', async () => {
      const refreshRequest = createRequest('tokenRefresh', token, {});

      logger.debug(refreshRequest.methodname + ' request');
      logger.debug(JSON.stringify(refreshRequest));

      const refreshResponse = await sendToWallet(refreshRequest);
      refreshResponseToken = refreshResponse.result.token;

      logger.debug(refreshRequest.methodname + ' response');
      logger.debug(JSON.stringify(refreshResponse));

      checkWalletResponse(refreshResponse, refreshRequest);
    });

    it('check that new token is authenticated', async () => {
      const refreshRequest = createRequest('tokenRefresh', token, {});
      const refreshResponse = await sendToWallet(refreshRequest);
      refreshResponseToken = refreshResponse.result.token;

      const getBalanceRequest = createRequest('getBalance', refreshResponse.result.token, {
        currency: config.playerCurrency,
        game_ref: GAME_REF,
      });

      logger.debug('After refreshing token' + getBalanceRequest.methodname + ' request');
      logger.debug(JSON.stringify(getBalanceRequest));

      const getBalanceResponse = await sendToWallet(getBalanceRequest);

      logger.debug('After refreshing token' + getBalanceRequest.methodname + ' response');
      logger.debug(JSON.stringify(getBalanceResponse));

      checkWalletResponse(getBalanceResponse, getBalanceRequest);
    });

    it('if new token after refresh than old token is obsolete', async () => {
      const refreshRequest = createRequest('tokenRefresh', token, {});
      const refreshResponse = await sendToWallet(refreshRequest);
      refreshResponseToken = refreshResponse.result.token;

      if (refreshResponse.result.token !== token) {
        const withdrawRequest = createRequest('withdraw', token, {
          amount: DEFAULT_WITHDRAW,
          currency: config.playerCurrency,
          game_ref: GAME_REF,
          round_id: uuid.v1(),
          action_id: uuid.v1()
        });
        const withdrawResponse = await sendToWallet(withdrawRequest);
        checkWalletResponse(withdrawResponse, withdrawRequest, {
          errorcode: ErrorCodes.INVALID_TOKEN
        });
      };
    });

    it('it is possible to refresh token two times in a row', async () => {
      const firstRefreshRequest = createRequest('tokenRefresh', token, {});

      logger.debug('First' + firstRefreshRequest.methodname + ' request');
      logger.debug(JSON.stringify(firstRefreshRequest));

      const firstRefreshResponse = await sendToWallet(firstRefreshRequest);
      refreshResponseToken = firstRefreshResponse.result.token;

      logger.debug('First' + firstRefreshResponse.methodname + ' response');
      logger.debug(JSON.stringify(firstRefreshResponse));

      const secondRefreshRequest = createRequest('tokenRefresh', firstRefreshResponse.result.token, {});

      logger.debug('Second ' + secondRefreshRequest.methodname + ' request');
      logger.debug(JSON.stringify(secondRefreshRequest));

      const secondRefreshResponse = await sendToWallet(secondRefreshRequest);
      refreshResponseToken = secondRefreshResponse.result.token;

      logger.debug('Second ' + secondRefreshRequest.methodname + ' response');
      logger.debug(JSON.stringify(secondRefreshResponse));

      checkWalletResponse(secondRefreshResponse, secondRefreshRequest);
    });
  });

  describe('offline deposit', () => {
    let roundId: string = undefined;
    let actionId: string = undefined;
    let token: string = undefined;
    let depositActionId: string = undefined;
    let rollbackActionId: string = undefined;

    beforeEach(async () => {
      roundId = uuid.v1();
      actionId = uuid.v1();
      depositActionId = uuid.v1();

      const authenticateRequest = createRequest('authenticateToken', initialToken, {});
      const authenticateResponse = await sendToWallet(authenticateRequest);
      token = authenticateResponse.result.token;
      const withdrawRequest = createRequest('withdraw', authenticateResponse.result.token, {
        amount: 0,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: actionId
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const withdrawResponse = await sendToWallet(withdrawRequest);

      logger.debug(withdrawResponse.methodname + ' response');
      logger.debug(JSON.stringify(withdrawResponse));

      rollbackActionId = withdrawRequest.args.action_id;
    });

    it('valid offline deposit', async () => {
      let offlineDepositRequest: BFJSONWSPRequest;
      offlineDepositRequest = createRequest('deposit', offlineToken(roundId, depositActionId, (config.isGamehub ? DEFAULT_DEPOSIT : undefined)), {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: depositActionId,
        offline: true
      });

      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const offlineDepositResponse = await sendToWallet(offlineDepositRequest);

      logger.debug(offlineDepositRequest.methodname + ' response');
      logger.debug(JSON.stringify(offlineDepositResponse));

      checkWalletResponse(offlineDepositResponse, offlineDepositRequest);
    });

    it('offline deposit works correctly after other actions', async () => {
      const refreshRequest = createRequest('tokenRefresh', token, {});
      const refreshResponse = await sendToWallet(refreshRequest);
      if (initialToken === token && initialToken !== refreshResponse.result.token ) {
        initialToken = refreshResponse.result.token;
      }

      const balanceRequest = createRequest('getBalance', token, {
        currency: config.playerCurrency,
        game_ref: GAME_REF,
      });
      await sendToWallet(balanceRequest);

      let offlineDepositRequest: BFJSONWSPRequest;
      offlineDepositRequest = createRequest('deposit', offlineToken(roundId, depositActionId, (config.isGamehub ? DEFAULT_DEPOSIT : undefined)), {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: depositActionId,
        offline: true
      });

      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const offlineDepositResponse = await sendToWallet(offlineDepositRequest);

      logger.debug(offlineDepositRequest.methodname + ' response');
      logger.debug(JSON.stringify(offlineDepositResponse));

      checkWalletResponse(offlineDepositResponse, offlineDepositRequest);
    });

    it('generated invalid offline token', async () => {
      const offlineDepositRequest = createRequest('deposit', 'invalidOfflineToken', {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1(),
        offline: true
      });

      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const offlineDepositResponse = await sendToWallet(offlineDepositRequest);

      logger.debug(offlineDepositRequest.methodname + ' response');
      logger.debug(JSON.stringify(offlineDepositResponse));

      checkWalletResponse(offlineDepositResponse, offlineDepositRequest, {
        errorcode: ErrorCodes.INVALID_TOKEN
      });
    });

    it('offlineDeposit without matching withdraw', async () => {
      const oddRoundId = uuid.v1();
      let offlineDepositRequest: BFJSONWSPRequest;
      offlineDepositRequest = createRequest('deposit', offlineToken(oddRoundId, depositActionId, (config.isGamehub ? DEFAULT_DEPOSIT : undefined)), {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: oddRoundId,
        action_id: depositActionId,
        offline: true
      });

      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const offlineDepositResponse = await sendToWallet(offlineDepositRequest);

      logger.debug(offlineDepositRequest.methodname + ' response');
      logger.debug(JSON.stringify(offlineDepositResponse));

      checkWalletResponse(offlineDepositResponse, offlineDepositRequest, {
        errorcode: ErrorCodes.TRANSACTION_NOT_FOUND
      });
    });

    it('duplicated offlineDeposit request', async () => {
      let offlineDepositRequest: BFJSONWSPRequest;
      offlineDepositRequest = createRequest('deposit', offlineToken(roundId, depositActionId, (config.isGamehub ? DEFAULT_DEPOSIT : undefined)), {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: depositActionId,
        offline: true
      });

      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const firstOfflineDepositResponse = await sendToWallet(offlineDepositRequest);

      logger.debug('First' + offlineDepositRequest.methodname + ' response');
      logger.debug(JSON.stringify(firstOfflineDepositResponse));

      checkWalletResponse(firstOfflineDepositResponse, offlineDepositRequest);

      const secondOfflineDepositResponse = await sendToWallet(offlineDepositRequest);

      logger.debug('Second ' + offlineDepositRequest.methodname + ' response on the same request');
      logger.debug(JSON.stringify(secondOfflineDepositResponse));

      if (secondOfflineDepositResponse.result.errorcode !== 0 && secondOfflineDepositResponse.result.errorcode !== undefined) {
        checkWalletResponse(secondOfflineDepositResponse, offlineDepositRequest, {
          errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED,
        });
      } else {
        expect(secondOfflineDepositResponse).to.deep.equal(firstOfflineDepositResponse);
      }
    });

    it('offline deposit with withdraw triggers end of the round, withdraw on ended round', async () => {
      let offlineDepositRequest: BFJSONWSPRequest;
      offlineDepositRequest = createRequest('deposit', offlineToken(roundId, depositActionId, (config.isGamehub ? DEFAULT_DEPOSIT : undefined)), {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: depositActionId,
        offline: true
      });

      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const offlineDepositResponse = await sendToWallet(offlineDepositRequest);

      logger.debug(offlineDepositResponse.methodname + ' response');
      logger.debug(JSON.stringify(offlineDepositResponse));

      checkWalletResponse(offlineDepositResponse, offlineDepositRequest);

      // withdraw on ended round
      const withdrawRequest = createRequest('withdraw', token, {
        amount: DEFAULT_WITHDRAW,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(withdrawRequest.methodname + ' request');
      logger.debug(JSON.stringify(withdrawRequest));

      const withdrawResponse = await sendToWallet(withdrawRequest);
      logger.debug(withdrawResponse.methodname + ' response');
      logger.debug(JSON.stringify(withdrawResponse));

      checkWalletResponse(withdrawResponse, withdrawRequest,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });

    it('offline deposit with withdraw triggers end of the round, offline deposit on ended round', async () => {
      let offlineDepositRequest: BFJSONWSPRequest;
      offlineDepositRequest = createRequest('deposit', offlineToken(roundId, depositActionId, (config.isGamehub ? DEFAULT_DEPOSIT : undefined)), {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: depositActionId,
        offline: true
      });

      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const offlineDepositResponseOnWithdraw = await sendToWallet(offlineDepositRequest);

      logger.debug(offlineDepositResponseOnWithdraw.methodname + ' response');
      logger.debug(JSON.stringify(offlineDepositResponseOnWithdraw));

      checkWalletResponse(offlineDepositResponseOnWithdraw, offlineDepositRequest);

      // offlineDeposit on ended round
      depositActionId = uuid.v1();
      offlineDepositRequest = createRequest('deposit', offlineToken(roundId, depositActionId, (config.isGamehub ? DEFAULT_DEPOSIT : undefined)), {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: depositActionId,
        offline: true
      });

      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const offlineDepositResponse = await sendToWallet(offlineDepositRequest);

      logger.debug(offlineDepositRequest.methodname + ' response');
      logger.debug(JSON.stringify(offlineDepositResponse));

      checkWalletResponse(offlineDepositResponse, offlineDepositRequest,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });

    it('offline deposit with withdraw triggers end of the round, deposit on ended round', async () => {
      let offlineDepositRequest: BFJSONWSPRequest;
      offlineDepositRequest = createRequest('deposit', offlineToken(roundId, depositActionId, (config.isGamehub ? DEFAULT_DEPOSIT : undefined)), {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: depositActionId,
        offline: true
      });

      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const offlineDepositResponseOnWithdraw = await sendToWallet(offlineDepositRequest);

      logger.debug(offlineDepositResponseOnWithdraw.methodname + ' response');
      logger.debug(JSON.stringify(offlineDepositResponseOnWithdraw));

      checkWalletResponse(offlineDepositResponseOnWithdraw, offlineDepositRequest);

      // deposit on ended round
      const depositRequest = createRequest('deposit', token, {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: uuid.v1()
      });

      logger.debug(depositRequest.methodname + ' request');
      logger.debug(JSON.stringify(depositRequest));

      const depositResponse = await sendToWallet(depositRequest);

      logger.debug(depositRequest.methodname + ' response');
      logger.debug(JSON.stringify(depositResponse));

      checkWalletResponse(depositResponse, depositRequest,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });

    it('offline deposit with withdraw triggers end of the round, rollback on ended round', async () => {
      let offlineDepositRequest: BFJSONWSPRequest;
      offlineDepositRequest = createRequest('deposit', offlineToken(roundId, depositActionId, (config.isGamehub ? DEFAULT_DEPOSIT : undefined)), {
        amount: DEFAULT_DEPOSIT,
        currency: config.playerCurrency,
        game_ref: GAME_REF,
        round_id: roundId,
        action_id: depositActionId,
        offline: true
      });
      logger.debug(offlineDepositRequest.methodname + ' request');
      logger.debug(JSON.stringify(offlineDepositRequest));

      const offlineDepositResponseOnWithdraw = await sendToWallet(offlineDepositRequest);

      logger.debug(offlineDepositResponseOnWithdraw.methodname + ' response');
      logger.debug(JSON.stringify(offlineDepositResponseOnWithdraw));

      checkWalletResponse(offlineDepositResponseOnWithdraw, offlineDepositRequest);

      // rollback on ended round
      const rollbackRequestOnEndedRound = createRequest('rollback', token, {
        round_id: roundId,
        rollback_action_id: rollbackActionId,
        action_id: uuid.v1(),
        game_ref: GAME_REF
      });

      logger.debug(rollbackRequestOnEndedRound.methodname + ' request');
      logger.debug(JSON.stringify(rollbackRequestOnEndedRound));

      const rollbackResponseFromEndedRound = await sendToWallet(rollbackRequestOnEndedRound);

      logger.debug(rollbackResponseFromEndedRound.methodname + ' response');
      logger.debug(JSON.stringify(rollbackResponseFromEndedRound));

      checkWalletResponse(rollbackResponseFromEndedRound, rollbackRequestOnEndedRound,
        { errorcode: ErrorCodes.ROUND_ALREADY_ENDED },
        { errorcode: ErrorCodes.TRANSACTION_ALREADY_PROCESSED }
      );
    });
  });
});