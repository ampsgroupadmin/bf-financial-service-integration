const chakram = require('chakram');
const expect = chakram.expect;

import * as crypto from 'crypto';

import config from './config';

import * as uuid from 'uuid';
import BFJSONWSPRequest from '../src/BFJSONWSPRequest';
import BFJSONWSPResponse from '../src/BFJSONWSPResponse';

const removeUndefinedKeys = (object: any) => {
  return Object.keys(object).reduce((obj: any, key: string) => {
    if (object[key] !== undefined) {
      obj[key] = object[key];
    }
    return obj;
  }, {});
};


const createRequest = (method: string, token: string, params?: any): BFJSONWSPRequest => ({
  args: {
    caller_id: config.callerId,
    caller_password: config.callerPassword,
    token: token,
    ...removeUndefinedKeys(params)
  },
  methodname: method,
  mirror: {
    id: uuid.v1()
  },
  type: 'jsonwsp/request',
  version: '1.0'
});

const sendToWallet = async (request: BFJSONWSPRequest): Promise<BFJSONWSPResponse> => {
  const response = await chakram.post(config.endpointUrl, request);
  expect(response, 'Incorrect HTTP code').to.have.status(HTTPStatusCode.OK);
  return response.body as BFJSONWSPResponse;
};

const checkWalletResponse = (response: BFJSONWSPResponse, request: BFJSONWSPRequest, expectedResultValues?: any, additionalErrorResult?: any) => {
  expect(response, 'Missing one of required keys [result, methodname, servicenumber, servicename, type, version, reflection]').to.have.all.keys('result', 'methodname', 'servicenumber', 'servicename', 'type', 'version', 'reflection');
  expect(response.methodname, `Incorrect methodname`).to.equal(request.methodname);
  expect(response.servicenumber, 'Incorrect servicenumber').to.equal(1);
  expect(response.servicename, 'Incorrect servicename').to.equal(config.servicename);
  expect(response.type, 'Incorrect type').to.equal('jsonwsp/response');
  expect(response.version, 'Incorrect version').to.equal('1.0');
  expect(response.reflection, 'Missing reflection.id').to.have.property('id');
  expect(response.reflection.id, 'Incorrect reflection id').to.equal(request.mirror.id);

  if (response.result.errorcode !== 0 && response.result.errorcode !== undefined && response.result.errorcode !== null) {
    expect(response.result, 'Missing errorcode').to.have.property('errorcode');

    if (expectedResultValues !== undefined && additionalErrorResult !== undefined) {
      expect(response.result.errorcode, 'Incorrect errorcode').to.be.oneOf([expectedResultValues.errorcode, additionalErrorResult.errorcode]);
      return;
    } else if (expectedResultValues !== undefined && additionalErrorResult === undefined) {
      expect(response.result.errorcode, 'Incorrect errorcode').to.equal(expectedResultValues.errorcode);
      return;
    }
    else {
      // This check is just for clean output to show the operator that we are waiting for 0/null but he sends us an error
      expect(response.result.errorcode, 'Incorrect errorcode').to.be.oneOf([0, null]);
    }
  }

  // This check is just for clean output to show the operator that we are waiting for an error but he sends us 0/null
  if (expectedResultValues !== undefined && expectedResultValues.errorcode !== undefined) {
    expect(response.result, 'Incorrect errorcode').to.include(expectedResultValues);
  }

  switch (request.methodname) {
    case 'authenticateToken':
      expect(response.result, 'Missing token').to.have.property('token');
      expect(response.result, 'Missing balance').to.have.property('balance');
      expect(response.result, 'Missing currency').to.have.property('currency');
      expect(response.result, 'Missing player_id').to.have.property('player_id');
      expect(response.result.player_id, 'Incorrect player_id - not a string').to.be.a('string');
      expect(Number.isInteger(response.result.balance), 'Incorrect balance - not an integer').to.equal(true);

      if (response.result.nickname !== undefined)
        expect(response.result, 'Missing nickname').to.have.property('nickname');

      break;
    case 'tokenRefresh':
      if (response.result.balance) {
        expect(Number.isInteger(response.result.balance), 'Incorrect balance - not an integer').to.equal(true);
      }
      break;
    case 'getBalance':
      expect(response.result, 'Missing balance').to.have.property('balance');
      expect(Number.isInteger(response.result.balance), 'Incorrect balance - not an integer').to.equal(true);
      break;
    case 'withdraw':
      expect(response.result, 'Missing balance').to.have.property('balance');
      expect(Number.isInteger(response.result.balance), 'Incorrect balance - not an integer').to.equal(true);
      expect(response.result.balance, 'Incorrect balance').to.equal(expectedResultValues.balance);

      expect(response.result, 'Missing transaction_id').to.have.property('transaction_id');
      break;
    case 'deposit':
      if (!request.args.offline) {
        expect(response.result, 'Missing balance').to.have.property('balance');
        expect(Number.isInteger(response.result.balance), 'Incorrect balance - not an integer').to.equal(true);
        expect(response.result.balance, 'Incorrect balance').to.equal(expectedResultValues.balance);
        expect(response.result, 'Missing currency').to.have.property('currency');
      }
      expect(response.result, 'Missing transaction_id').to.have.property('transaction_id');
      break;
    case 'rollback':
      expect(response.result, 'Missing transaction_id').to.have.property('transaction_id');
      break;
  }

  if (response.result.currency !== undefined) {
    expect(response.result, 'Missing currency').to.have.property('currency');
    expect(response.result.currency, 'Incorrect currency').to.equal(response.result.currency.toUpperCase());
    expect(response.result.currency, 'Incorrect currency').to.have.lengthOf(3);
    expect(response.result.currency, 'Incorrect currency').to.equal(config.playerCurrency);
  } else if (response.result.currency !== undefined && request.args.currency !== undefined) {
    expect(response.result.currency, 'Incorrect currency').to.equal(request.args.currency);
  }

  if (response.result.token) {
    expect(response.result, 'Missing token').to.have.property('token');
  }
};

const offlineToken = (roundId: string, actionId: string, amount?: number) => {
  let data: string;
  if (config.isGamehub) {
    data = `${config.offlineTokenKey}${config.callerId}${roundId}${actionId}${amount.toString()}`;
    return crypto.createHash('sha224').update(data).digest('hex') + '_' + config.playerId;
  } else {
    data = `${config.offlineTokenKey}${config.callerId}${roundId}${actionId}`;
    return crypto.createHash('sha224').update(data).digest('hex') + '_' + config.playerId;
  }
};

enum ErrorCodes {
  SUCCESS = 0,

  // General errors
  GENERAL_API = 1000,
  CURRENCY_TRANSFER = 1001,
  WALLET_LOCK = 1002,
  INVALID_METHOD_NAME = 1003,
  GENERAL_API_WITHOUT_ROLLBACK = 1004,

  // Authentication errors
  GENERAL_AUTHENTICATION = 2000,
  INVALID_TOKEN = 2001,
  INVALID_PLAYER = 2002,
  NOT_LOGGED_IN = 2003,

  // Transaction errors
  TRANSACTION_ALREADY_PROCESSED = 3000,
  TRANSACTION_NOT_FOUND = 3001,
  ROUND_ALREADY_ENDED = 3002,

  // Player funds error
  PLAYER_INSUFFICIENT_FUNDS = 4000,
  EXCEEDED_LOSS_LIMIT = 4001,
  EXCEEDED_WAGER_LIMIT = 4002,
  EXCEDEED_GAME_SESSION_TIME_LIMIT = 4003,
  EXCEEDED_LIMIT = 4004
}

enum HTTPStatusCode {
  OK = 200,
  CREATED = 201,
  EMPTY = 204,
  BAD_REQUEST = 400,
  FORBIDDEN = 403,
  NOT_FOUND = 404,
  RESOURCE_EXIST = 409,
  CONFLICT = 409,
  SERVER_ERROR = 500
}

export {
  offlineToken,
  checkWalletResponse,
  sendToWallet,
  createRequest,
};

export {
  HTTPStatusCode,
  ErrorCodes
};
