import * as uuid from 'uuid';

// Our test data
export default {
  callerId: 'beefee', // caller used in the service requests
  callerPassword: 'SuperPassword!', // caller password used in the service requests

  servicename: 'BFGamesSeamless',
  game_ref: '93823fdf-d73e-4f9d-9eeb-06568bbba31e',

  playerId: 'joe', // id of the player used for tests
  playerNickname: 'Joe Doe',
  playerCurrency: 'EUR', // currency ISO used for tests
  offlineTokenKey: 'testToken', // secret used in offline token generation (as explained in the documentation)
  endpointUrl: 'http://localhost:8000/jsonwsp', // jsonwsp service endpoint
  tokenUrl: 'http://localhost:8000/token', // test REST GET endpoint returning token used for testing
  token: undefined, // Token used for tests, has to be specified if token endpoint is not available

  isDebug: true, // t ue to show logs in console
  isGamehub: true,  // true if the integration is done via gamehub
  isBonusFunctionality: false, // true if the bonus functionality is implemented
  bonusProgramId: uuid.v1(), // replace with values corresponding to the bonus created on your side or set to undefined
  bonusInstanceId: uuid.v1(), // replace with values corresponding to the bonus created on your side or set to undefined
  bonusSystemId: 'test' // replace with values corresponding to the bonus created on your side or set to undefined
};